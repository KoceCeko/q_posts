import type { Post } from "@/types/post-type";
import { describe, expect, it } from "vitest";
import { mount } from "@vue/test-utils";
import PostElement from "@/components/PostElement.vue";
import type { ComponentCustomProps } from "vue";

const postTest = {
  id: 0,
  title: "post title",
  body: "post body",
  userId: 0,
} as Post;

function postFactory(props: ComponentCustomProps) {
  return mount(PostElement, { ...props });
}

describe("PostElement", () => {
  it("renders post", () => {
    const wrapper = postFactory({ props: { post: postTest } });
    expect(wrapper.html()).toContain("post title");
    expect(wrapper.html()).toContain("post body");
  });

  it("toggle comments", async () => {
    const wrapper = mount(PostElement, { props: { post: postTest } });
    await wrapper.find("button").trigger("click");
    expect(wrapper.html()).toContain("comments-list");
  });
});
