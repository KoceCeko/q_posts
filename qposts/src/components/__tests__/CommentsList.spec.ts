import type { Commenet as PostComment } from "@/types/post-type";
import { describe, expect, it } from "vitest";
import { mount } from "@vue/test-utils";
import CommentsList from "@/components/CommentsList.vue";
import type { ComponentCustomProps } from "vue";

const mocComments = [
  {
    id: 0,
    name: "first",
    email: "email@mail.com",
    body: "test comment body",
  },
  {
    id: 2,
    name: "2nd",
    email: "email2@mail.com",
    body: "test comment body",
  },
] as PostComment[];

function commentFactory(props: ComponentCustomProps) {
  return mount(CommentsList, {
    ...props,
    data: () => {
      return { comments: mocComments, loading: false, error: undefined };
    },
  });
}

describe("CommentsList", () => {
  it("renders properly", () => {
    const wrapper = commentFactory({ props: { postId: 0 } });
    expect(wrapper.isVisible()).toBe(true);
    expect(wrapper.findAll("li").length).toBe(2);
  });
});
