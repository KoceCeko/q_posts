import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Home",
      redirect: () => {
        return { path: "/posts" };
      },
    },
    {
      path: "/posts",
      name: "Posts",
      component: () => import("@/views/PostView.vue"),
    },
    {
      path: "/post/:id",
      name: "Post details",
      component: () => import("@/views/PostDetailsView.vue"),
    },
    {
      path: "/:pathMatch(.*)*",
      name: "404",
      component: () => import("@/views/NotFound.vue"),
    },
  ],
});

export default router;
