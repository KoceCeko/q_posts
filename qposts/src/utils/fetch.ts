export interface ResponseData<T> {
  data?: T;
  loading: boolean;
  error?: string;
}

const BASE_URL = "https://jsonplaceholder.typicode.com/";

export function fetchData<T>(
  endpoint: string,
  params: Record<string, string | string> | undefined = undefined
): Promise<ResponseData<T>> {
  const url =
    BASE_URL + endpoint + (params ? "?" + new URLSearchParams(params) : "");
  console.log("URL", url);

  const response = fetch(url)
    .then((res) => {
      if (res.status != 200) {
        throw new Error("Failed to fatch data");
      }
      return res.json();
    })
    .then((data: T) => {
      return { data: data, loading: false, error: undefined };
    })
    .catch((err) => {
      return { data: undefined, loading: false, error: err };
    });

  return response;
}
