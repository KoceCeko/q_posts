import { reactive } from "vue";
import type { User } from "@/types/user-type";
import type { Post } from "@/types/post-type";

interface StoreType<T> {
  loading: boolean;
  error: undefined | string;
  data: T;
}

const initUserState = {
  data: {} as Record<number, User>,
  loading: false,
  error: undefined as string | undefined,
} as StoreType<Record<number, User>>;

const initPostState = {
  data: [] as Post[],
  loading: false,
  error: undefined as string | undefined,
} as StoreType<Post[]>;

export const userStore = reactive(initUserState);
export const postsStore = reactive(initPostState);
