import type { Post } from "@/types/post-type";
import { postsStore, userStore } from "@/utils/store";

export function filterPosts(filter: string, posts: Post[] = postsStore.data) {
  const loweredFilter = filter.toLowerCase();
  return posts.filter(
    (post) =>
      post.body.toLowerCase().includes(loweredFilter) ||
      post.title.toLowerCase().includes(loweredFilter)
  );
}

export function filterPostsByUserName(
  filter: string,
  posts: Post[] = postsStore.data
) {
  const loweredFilter = filter.toLowerCase();
  const user = Object.values(userStore.data).find(
    (usr) =>
      usr.name.toLowerCase().includes(loweredFilter) ||
      usr.username.toLowerCase().includes(loweredFilter)
  );
  if (user) {
    return posts.filter((post) => post.userId == user.id);
  }
  return [];
}
