# Q-test-project

This application is used for skill testing provided by an assignment.

## Requirements

```
docker-compose
node >=8.19
```
This project is done on linux sistem.

## Installation and run

```bash
cd $root_dir/qposts
npm i
cd ..
docker-compose build
docker-compose up
```

if docker-compose fails to run, use:
```
cd %root_dir%/qposts
npm install
npm run dev
```

### Type-Check

```bash
npm run type-check
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```bash
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```bash
npm run lint
```